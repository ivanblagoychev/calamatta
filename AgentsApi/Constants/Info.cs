﻿namespace AgentsApi.Constants
{
    public static class Info
    {
        public const string NoAgentsAvailable = "No available agents.";

        public const string NoActiveSessionsPresent = "No active session present";

        public const string SendingMessageToQueue = "Sending message to queue service.";

        public const string ReceivedMessage = "Received message from queue service.";
    }
}