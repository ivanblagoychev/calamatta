﻿namespace AgentsApi.Constants
{
    public static class Queries
    {
        public const string AssignChatSessionToAgent =
            @"update public.chat_session set status = @status, assigned_at = @assignedAt where agent_id = @agentid and id = @id;";

        public const string CloseChatSession =
            @"update public.chat_session set status = @status, closed_at = @closed_at where id = @id;";

        public const string CloseAllSessionsForTeam =
            //@"update public.chat_session set status = @status, closed_at = @closed_at where agent_id = @agentid and closed_at is null and status != @statusDenied;";
            @"update public.chat_session
                set status = @status, closed_at = @closed_at where
                agent_id in
                (select a.id from public.agent as a
                join public.team as t on t.id = a.team_id
                where a.team_id in (@teamId, @overflowTeamId));";

        public const string GetAllOpenedChatSessionForTeam =
            // @"select * from public.chat_session as cs
            //         join public.agent as a on a.id = cs.agent_id
            //         join public.team as t on a.team_id = t.id
            //         where t.id = @team_id and cs.status = @status;";

            @"select * from public.chat_session where team_id = @team_id and status = @status;";

        public const string GetFirstPossibleAgentToAssignSession = 
            @"select cs.agent_id from public.chat_session as cs
                join agent a on cs.agent_id = a.id
                join team t on a.team_id = t.id
                where cs.status = @status and t.id = @teamId
                group by cs.agent_id
                having count(cs.id) < 10 * @multiplier
                order by count(cs.id) desc limit 1;";

        public const string UpdateSessionToAssigned =
            @"update public.chat_session
                set assigned_at = @assignedAt, agent_id = @agentId, team_id = @teamId, status = @status
                where id = @sessionId;";

        public const string GetUnassignedSessions =
            @"select * from public.chat_session where agent_id is null and status = @status
                order by queued_at;";

        public const string GetAgentIdFromSession =
            @"select * from public.chat_session where id = @sessionId;";

        public const string GetAllMmbersOfTeam =
            @"select * from public.agent where team_id = @teamId;";

        public const string GetCurrentTeamInfo =
            @"select * from public.team where id = @teamId;";
    }
}