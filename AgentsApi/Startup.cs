namespace AgentsApi
{
    using Shared;
    using Shared.Extensions;
    using Shared.Services;
    using Enum;
    using Interfaces;
    using Services;
    using Dapper;
    using BackgroundJobs;
    using Settings;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class Startup
    {
        public Startup(IConfiguration cfg)
        {
            Configuration = cfg;
        }
        
        private IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services
                .Configure<MultiplierSettings>(Configuration.GetSection(ConfigSection.Multipliers.ToString()))
                .Configure<DbSettings>(Configuration.GetSection(ConfigSection.DbSettings.ToString()))
                .Configure<WorkHoursSettings>(Configuration.GetSection(ConfigSection.WorkingHours.ToString()))
                .Configure<KafkaConsumerSettings>(Configuration.GetSection(ConfigSection.KafkaConsumer.ToString()))
                .Configure<KafkaClientSettings>(Configuration.GetSection(ConfigSection.KafkaClient.ToString()));
            
            services.AddHostedService<KafkaConsumerJob>();

            services.AddTransient<IAgentService, AgentService>()
                .AddTransient<IDbOperator, DbOperator>()
                .AddTransient<IKafkaClientService, KafkaClientService>()
                .AddTransient<ISimpleLogger, SimpleLogger>();
            
            DefaultTypeMap.MatchNamesWithUnderscores = true;
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();
            app.UseEndpoints(e => e.MapControllers());
            app.UseUnhandledExceptionHandling();
        }
    }
}