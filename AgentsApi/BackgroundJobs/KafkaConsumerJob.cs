namespace AgentsApi.BackgroundJobs
{
    using Constants;
    using Shared;
    using Settings;
    using Microsoft.Extensions.Options;
    using Models;
    using Newtonsoft.Json;
    using Interfaces;
    using Confluent.Kafka;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Hosting;
    
    public class KafkaConsumerJob : BackgroundService
    {
        public KafkaConsumerJob(IAgentService serv, IOptions<KafkaConsumerSettings> settings, ISimpleLogger logg)
        {
            Service = serv;
            Settings = settings.Value;
            Logger = logg;
        }

        private KafkaConsumerSettings Settings { get; }
        
        private IAgentService Service { get; } 
        
        private ISimpleLogger Logger { get; }
        
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Run(async () =>
            {
                var config = new ConsumerConfig()
                {
                    BootstrapServers = Settings.Server,
                    GroupId = Settings.GroupId,
                    AutoOffsetReset = AutoOffsetReset.Earliest
                };

                using (var consumer = new ConsumerBuilder<Ignore, string>(config).Build())
                {
                    consumer.Subscribe(Settings.Topic);
                    while (!stoppingToken.IsCancellationRequested)
                    {
                        var consumedData = consumer.Consume();

                        Logger.LogInfo(Info.ReceivedMessage);
                        var req = JsonConvert.DeserializeObject<QueueAgentRequest>(consumedData.Message.Value);

                        if (req.TeamId != 0)
                        {
                            await Service.CloseAllSessionsForTeam(req.TeamId);
                        }
                        else if (req.SessionId != 0)
                        {
                            await Service.CloseSpecificSessionAndAssignNext(req.SessionId);
                        }
                        else await Service.AssignRequest(req.Session);
                    }
                }
            }, stoppingToken);
        }
    }
}