﻿namespace AgentsApi.Interfaces
{
    using System.Threading.Tasks;
    using Shared.Models.ResponseModels;
    
    public interface IKafkaClientService
    {
        Task SendResponseToQueueService(ApiResponse resp);
    }
}