namespace AgentsApi.Interfaces
{
    using System.Threading.Tasks;
    using Shared.Models;
    
    public interface IAgentService
    {
        Task AssignRequest(ChatSession session);

        Task CloseSpecificSessionAndAssignNext(long sessionId);

        Task CloseAllSessionsForTeam(int teamId);
    }
}