﻿namespace AgentsApi.Interfaces
{
    using Models.DbModels;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    
    public interface IDbOperator
    {
        Task AssigneSessionToAgent(int agentId, int teamId, long sessionId);

        Task CloseChatSession(long sessionId);

        Task CloseAllSessionsForTeam(int teamId);

        Task<IList<ChatSessionModel>> GetActiveSessionsForTeam(int teamId);

        Task<int> GetTheFirstPossibleAgents(int teamId, decimal multiplier);

        Task<ChatSessionModel> GetUnassignedSessions();

        Task<ChatSessionModel> GetAgentIdFromSession(long sessionId);

        Task<IList<AgentModel>> GetTeamMembers(int teamId);

        Task<TeamModel> GetTeamInfo(int teamId);
    }
}