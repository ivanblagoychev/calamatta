﻿namespace AgentsApi.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using Models.DbModels;
    using Shared.Enums;
    using System;
    using System.Threading.Tasks;
    using Constants;
    using Interfaces;
    using Settings;
    using Dapper;
    using Microsoft.Extensions.Options;
    using Npgsql;
    
    public class DbOperator : IDbOperator
    {
        public DbOperator(IOptions<DbSettings> settings)
        {
            Settings = settings.Value;
        }
        
        private DbSettings Settings { get; set; }
        
        public async Task ChangeSessionStatusAsigned(long sessionId, int agentId)
        {
            using (var conn = new NpgsqlConnection(Settings.ConnectionString))
            {
                conn.Open();

                using (var tran = conn.BeginTransaction())
                {
                    await conn.ExecuteAsync(Queries.AssignChatSessionToAgent, new
                    {
                        status = (int) ChatSessionStatus.Assigned,
                        assignedAt = DateTime.Now,
                        agentid = agentId,
                        id = sessionId
                    });
                }
            }
        }

        public async Task CloseChatSession(long sessionId)
        {
            using (var conn = new NpgsqlConnection(Settings.ConnectionString))
            {
                conn.Open();

                using (var tran = conn.BeginTransaction())
                {
                    await conn.ExecuteAsync(Queries.CloseChatSession, new
                    {
                        status = (int) ChatSessionStatus.Closed,
                        closed_at = DateTime.Now,
                        id = sessionId
                    });
                }
            }
        }

        public async Task CloseAllSessionsForTeam(int teamId)
        {
            using (var conn = new NpgsqlConnection(Settings.ConnectionString))
            {
                conn.Open();

                using (var tran = conn.BeginTransaction())
                {
                    await conn.ExecuteAsync(Queries.CloseAllSessionsForTeam, new
                    {
                        status = (int) ChatSessionStatus.Closed,
                        closed_at = DateTime.Now,
                        teamId = teamId,
                        overflowTeamId = (int) Team.Overflow
                    });

                    tran.Commit();
                }
            }
        }

        public async Task<IList<ChatSessionModel>> GetActiveSessionsForTeam(int teamId)
        {
            using (var conn = new NpgsqlConnection(Settings.ConnectionString))
            {
                conn.Open();

                var sessions = new List<ChatSessionModel>();
                
                sessions =  (await conn.QueryAsync<ChatSessionModel>
                (Queries.GetAllOpenedChatSessionForTeam, new
                {
                    team_id = teamId,
                    status = (int) ChatSessionStatus.Assigned
                })).ToList();

                return sessions;
            }
        }

        public async Task<int> GetTheFirstPossibleAgents(int teamId, decimal multiplier)
        {
            using (var conn = new NpgsqlConnection(Settings.ConnectionString))
            {
                conn.Open();

                var agentId = Convert.ToInt32(await conn.QuerySingleAsync<int>(
                    Queries.GetFirstPossibleAgentToAssignSession, new
                    {
                        status = (int) ChatSessionStatus.Assigned,
                        teamId = teamId,
                        multiplier = multiplier
                    }));
                
                return agentId > 0 ? agentId : 0;
            }
        }

        public async Task AssigneSessionToAgent(int agentId, int teamId, long sessionId)
        {
            using (var conn = new NpgsqlConnection(Settings.ConnectionString))
            {
                conn.Open();

                using (var tran = conn.BeginTransaction())
                {
                    await conn.ExecuteAsync(Queries.UpdateSessionToAssigned, new
                    {
                        agentId = agentId,
                        assignedAt = DateTime.Now,
                        sessionId = sessionId,
                        teamId = teamId,
                        status = (int) ChatSessionStatus.Assigned
                    });
                    
                    tran.Commit();
                }
            }
        }

        public async Task<ChatSessionModel> GetUnassignedSessions()
        {
            using (var conn = new NpgsqlConnection(Settings.ConnectionString))
            {
                conn.Open();

                var unassignedSessions =  (await conn.QueryAsync<ChatSessionModel>(Queries.GetUnassignedSessions, new
                {
                    status = (int) ChatSessionStatus.Queued
                })).ToList();

                return unassignedSessions.Any() ? unassignedSessions[1] : null;
            }
        }

        public async Task<ChatSessionModel> GetAgentIdFromSession(long sessionId)
        {
            using (var conn = new NpgsqlConnection(Settings.ConnectionString))
            {
                conn.Open();

                return (await conn.QuerySingleAsync<ChatSessionModel>(Queries.GetAgentIdFromSession, new { sessionId = sessionId }));
            }
        }

        public async Task<IList<AgentModel>> GetTeamMembers(int teamId)
        {
            using (var conn = new NpgsqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                
                return (await conn.QueryAsync<AgentModel>(Queries.GetAllMmbersOfTeam, new { teamId = teamId})).ToList();
            }
        }

        public async Task<TeamModel> GetTeamInfo(int teamId)
        {
            using (var conn = new NpgsqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return (await conn.QueryFirstOrDefaultAsync<TeamModel>(Queries.GetCurrentTeamInfo, new { teamId = teamId}));
            }
        }
    }
}