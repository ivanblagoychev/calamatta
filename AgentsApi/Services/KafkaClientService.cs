﻿namespace AgentsApi.Services
{
    using Constants;
    using Shared;
    using Settings;
    using Microsoft.Extensions.Options;
    using System.Threading.Tasks;
    using Confluent.Kafka;
    using Newtonsoft.Json;
    using Shared.Models.ResponseModels;
    using Interfaces;
    
    public class KafkaClientService : IKafkaClientService
    {
        public KafkaClientService(IOptions<KafkaClientSettings> settings, ISimpleLogger logg)
        {
            Settings = settings.Value;
            Logger = logg;
        }
        
        private KafkaClientSettings Settings { get; }
        
        private ISimpleLogger Logger { get; set; }
        
        public async Task SendResponseToQueueService(ApiResponse resp)
        {
            var config = new ProducerConfig
            {
                BootstrapServers = Settings.Server
            };

            var msg = JsonConvert.SerializeObject(resp);
            
            using (var producer = new ProducerBuilder<Null, string>(config).Build())
            {
                Logger.LogInfo(Info.SendingMessageToQueue);
                await producer.ProduceAsync(Settings.Topic, new Message<Null, string> { Value= msg });
                producer.Flush();
            }
        }
    }
}