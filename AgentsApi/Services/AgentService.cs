namespace AgentsApi.Services
{
    using Shared;
    using Constants;
    using Settings;
    using Microsoft.Extensions.Options;
    using Shared.Models.ExceptionsModels;
    using Shared.Models.ResponseModels;
    using Shared.Enums;
    using Shared.Models;
    using System.Collections.Generic;
    using System.Linq;
    using Models.DbModels;
    using System;
    using System.Threading.Tasks;
    using Interfaces;

    public class AgentService : IAgentService
    {
        public AgentService(IDbOperator dbOperator, IKafkaClientService kafkaService,
            IOptions<WorkHoursSettings> whSettings, IOptions<MultiplierSettings> mpSettings, ISimpleLogger log)
        {
            DbOperator = dbOperator;
            Kafka = kafkaService;
            WorkHourSettings = whSettings.Value;
            MultiplierSettings = mpSettings.Value;
            Logger = log;
        }

        private IDbOperator DbOperator { get; }

        private IKafkaClientService Kafka { get; }

        private WorkHoursSettings WorkHourSettings { get; }

        private MultiplierSettings MultiplierSettings { get; }

        private ISimpleLogger Logger { get; }

        public async Task AssignRequest(ChatSession session)
        {
            try
            {
                var resp = new ApiResponse();
                var currentOperatingTeam = GetCurrentTeam();
                var teamMembers = await DbOperator.GetTeamMembers((int) currentOperatingTeam);

                IList<ChatSessionModel> activeSessions =
                    await DbOperator.GetActiveSessionsForTeam((int) currentOperatingTeam);

                if (!activeSessions.Any())
                {
                    bool isAgentChosen = false;
                    int chosenAgentId = 0;

                    if (teamMembers.Any(x => x.SeniorityId == 1))
                    {
                        chosenAgentId = teamMembers.Where(m => m.SeniorityId == 1).OrderBy(a => a.Id).FirstOrDefault()
                            .Id;
                        isAgentChosen = true;
                    }

                    if (teamMembers.Any(x => x.SeniorityId == 2) && !isAgentChosen)
                    {
                        chosenAgentId = teamMembers.Where(m => m.SeniorityId == 2).OrderBy(a => a.Id).FirstOrDefault()
                            .Id;
                        isAgentChosen = true;
                    }

                    if (teamMembers.Any(x => x.SeniorityId == 3) && !isAgentChosen)
                    {
                        chosenAgentId = teamMembers.Where(m => m.SeniorityId == 3).OrderBy(a => a.Id).FirstOrDefault()
                            .Id;
                        isAgentChosen = true;
                    }

                    if (teamMembers.Any(x => x.SeniorityId == 4) && !isAgentChosen)
                    {
                        chosenAgentId = teamMembers.Where(m => m.SeniorityId == 4).OrderBy(a => a.Id).FirstOrDefault()
                            .Id;
                    }

                    await DbOperator.AssigneSessionToAgent(chosenAgentId, (int) currentOperatingTeam, session.Id);
                    resp.Error = null;
                    resp.IsSuccess = true;
                    resp.ChatSessionId = session.Id;
                    await Kafka.SendResponseToQueueService(resp);

                    return;
                }

                var currentTeamInfo = await DbOperator.GetTeamInfo((int) currentOperatingTeam);

                var maxSessionsForTeam = currentTeamInfo.MaxSessions;

                if (activeSessions.Count >= maxSessionsForTeam && !IsBetweenWorkHours())
                {
                    resp.Error = new ApiException(Info.NoAgentsAvailable, null);
                    resp.IsSuccess = false;
                    resp.ChatSessionId = session.Id;

                    await Kafka.SendResponseToQueueService(resp);
                    return;
                }

                if (activeSessions.Count >= maxSessionsForTeam && IsBetweenWorkHours())
                {
                    activeSessions = await DbOperator.GetActiveSessionsForTeam((int) Team.Overflow);
                    currentTeamInfo = await DbOperator.GetTeamInfo((int) Team.Overflow);

                    if (activeSessions.Count >= currentTeamInfo.MaxSessions)
                    {
                        resp.Error = new ApiException(Info.NoAgentsAvailable, null);
                        resp.IsSuccess = false;
                        resp.ChatSessionId = session.Id;

                        await Kafka.SendResponseToQueueService(resp);
                        return;
                    }

                    teamMembers = await DbOperator.GetTeamMembers((int) Team.Overflow);

                    if (activeSessions.Count == 0)
                    {
                        await DbOperator.AssigneSessionToAgent(teamMembers.FirstOrDefault().Id, (int) Team.Overflow, session.Id);

                        resp.Error = null;
                        resp.IsSuccess = true;
                        resp.ChatSessionId = session.Id;
                        await Kafka.SendResponseToQueueService(resp);

                        return;
                    }

                    var availableOverflowAgentId =
                        await DbOperator.GetTheFirstPossibleAgents((int) Team.Overflow, MultiplierSettings.Juniors);

                    int chosenAgentId = 0;

                    foreach (var agent in teamMembers)
                    {
                        if (activeSessions.Where(s => s.AgentId == agent.Id).ToList().Count() < 10 * 0.4)
                        {
                            chosenAgentId = agent.Id;
                            break;
                        }
                    }

                    await DbOperator.AssigneSessionToAgent(chosenAgentId, (int) Team.Overflow, session.Id);

                    resp.Error = null;
                    resp.IsSuccess = true;
                    resp.ChatSessionId = session.Id;
                    await Kafka.SendResponseToQueueService(resp);
                }
                else
                {
                    bool isAgentChosen = false;
                    var chosenAgentId = 0;

                    if (currentTeamInfo.Juniors > 0)
                    {
                        foreach (var junior in teamMembers.Where(m => m.SeniorityId == 1).ToList())
                        {
                            if (activeSessions.Where(s => s.AgentId == junior.Id).ToList().Count() < 10 * 0.4)
                            {
                                chosenAgentId = junior.Id;
                                isAgentChosen = true;
                                break;
                            }
                        }
                    }

                    if (currentTeamInfo.MidLevels > 0 && !isAgentChosen)
                    {
                        foreach (var midLevel in teamMembers.Where(m => m.SeniorityId == 2).ToList())
                        {
                            if (activeSessions.Where(s => s.AgentId == midLevel.Id).ToList().Count() < 10 * 0.6)
                            {
                                chosenAgentId = midLevel.Id;
                                isAgentChosen = true;
                                break;
                            }
                        }
                    }

                    if (currentTeamInfo.Seniors > 0 && !isAgentChosen)
                    {
                        foreach (var senior in teamMembers.Where(m => m.SeniorityId == 3).ToList())
                        {
                            if (activeSessions.Where(s => s.AgentId == senior.Id).ToList().Count() < 10 * 0.8)
                            {
                                chosenAgentId = senior.Id;
                                isAgentChosen = true;
                                break;
                            }
                        }
                    }

                    if (currentTeamInfo.TeamLeads > 0 && !isAgentChosen)
                    {
                        foreach (var lead in teamMembers.Where(m => m.SeniorityId == 4).ToList())
                        {
                            if (activeSessions.Where(s => s.AgentId == lead.Id).ToList().Count() < 10 * 0.5)
                            {
                                chosenAgentId = lead.Id;
                                isAgentChosen = true;
                                break;
                            }
                        }
                    }

                    await DbOperator.AssigneSessionToAgent(chosenAgentId, (int) currentOperatingTeam, session.Id);

                    resp.Error = null;
                    resp.IsSuccess = true;
                    resp.ChatSessionId = session.Id;
                    await Kafka.SendResponseToQueueService(resp);
                }
            }
            catch (Exception e)
            {
                Logger.LogInfo(e.Message);
            }
        }
        
        private bool IsBetweenWorkHours()
        {
            var hourFrom = TimeSpan.Parse(WorkHourSettings.WorkingHoursFrom);
            var hourTo = TimeSpan.Parse(WorkHourSettings.WorkingHoursTo);

            return DateTime.Now.TimeOfDay >= hourFrom && DateTime.Now.TimeOfDay <= hourTo;
        }

        private Team GetCurrentTeam()
        {
            var now = DateTime.Now.TimeOfDay;

            if (now >= new TimeSpan(0, 0, 0) && now <= new TimeSpan(7, 59, 59))
                return Team.C;

            if (now >= new TimeSpan(8, 0, 0) && now <= new TimeSpan(15, 59, 59))
                return Team.A;

            return Team.B;
        }

        public async Task CloseSpecificSessionAndAssignNext(long sessionId)
        {
            try
            {
                var sessionInfo = await DbOperator.GetAgentIdFromSession(sessionId);
                await DbOperator.CloseChatSession(sessionId);

                var unAssignedSessions = await DbOperator.GetUnassignedSessions();
                if (unAssignedSessions != null)
                {
                    await DbOperator.AssigneSessionToAgent(sessionInfo.AgentId, sessionInfo.TeamId, unAssignedSessions.Id);
                }
            }
            catch (Exception e)
            {
                Logger.LogInfo(e.Message);
            }
        }

        public async Task CloseAllSessionsForTeam(int teamId)
        {
            try
            {
                await DbOperator.CloseAllSessionsForTeam(teamId);
            }
            catch (Exception e)
            {
                Logger.LogInfo(e.Message);
            }
        }
    }
}