﻿namespace AgentsApi.Enum
{
    public enum ConfigSection
    {
        WorkingHours,
        KafkaConsumer,
        KafkaClient,
        Multipliers,
        DbSettings
    }
}