﻿namespace AgentsApi.Models.DbModels
{
    public class TeamModel : DbModel
    {
        public string Name { get; set; }
        
        public int Juniors { get; set; }
        
        public int MidLevels { get; set; }
        
        public int Seniors { get; set; }
        
        public int TeamLeads { get; set; }
        
        public int MaxSessions { get; set; }
    }
}