﻿namespace AgentsApi.Models.DbModels
{
    public class SeniorityModel : DbModel
    {
        public string Name { get; set; }
        
        public decimal Multiplier { get; set; }
    }
}