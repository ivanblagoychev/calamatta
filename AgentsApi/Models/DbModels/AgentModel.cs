﻿namespace AgentsApi.Models.DbModels
{
    public class AgentModel : DbModel
    {
        public int TeamId { get; set; }
        
        public int SeniorityId { get; set; }
        
        //public TeamModel Team { get; set; }
        
        //public SeniorityModel Seniority { get; set; }
    }
}