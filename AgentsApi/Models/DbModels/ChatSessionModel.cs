﻿namespace AgentsApi.Models.DbModels
{
    using System;
    using Shared.Enums;
    
    public class ChatSessionModel : DbModel
    {
        public ChatSessionStatus Status { get; set; }
        
        public DateTime RequestedAt { get; set; }
        
        public DateTime QueuedAt { get; set; }
        
        public DateTime AssignedAt { get; set; }
        
        public DateTime ClosedAt { get; set; }
        
        public int AgentId { get; set; }
        
        public AgentModel Agent { get; set; } 
        
        public int TeamId { get; set; }
        
        public TeamModel Team { get; set; }
    }
}