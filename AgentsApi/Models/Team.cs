namespace AgentsApi.Models
{
    public class Team
    {
        public int Juniors { get; set; }
        public int MidLevels { get; set; }
        public int Seniors { get; set; }
        public int TeamLeads { get; set; }
    }
}