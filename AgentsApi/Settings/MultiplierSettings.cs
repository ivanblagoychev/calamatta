namespace AgentsApi.Settings
{
    public class MultiplierSettings
    {
        public decimal Juniors { get; set; }
        public decimal MidLevels { get; set; }
        public decimal Seniors { get; set; }
        public decimal TeamLeads { get; set; }
        
        public decimal Concurency { get; set; }
    }
}