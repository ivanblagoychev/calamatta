﻿namespace AgentsApi.Settings
{
    public class KafkaSettings
    {
        public string Server { get; set; }
        
        public string GroupId { get; set; }
        
        public string Topic { get; set; }
    }

    public class KafkaConsumerSettings : KafkaSettings
    { }

    public class KafkaClientSettings : KafkaSettings
    { }
}