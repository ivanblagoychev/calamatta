﻿namespace AgentsApi.Settings
{
    public class WorkHoursSettings
    {
        public string WorkingHoursFrom { get; set; }

        public string WorkingHoursTo { get; set; }
    }
}