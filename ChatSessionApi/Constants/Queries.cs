﻿namespace ChatSessionApi.Constants
{
    public static class Queries
    {
        public const string CreateNewChatSession = 
            "insert into public.chat_session(status, requested_at) values (@status, @requestedAt) returning id;";
    }
}