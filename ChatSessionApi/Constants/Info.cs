namespace ChatSessionApi.Constants
{
    public class Info
    {
        public const string ReceivedNewChatSession = "Recieved a new chat session request.";

        public const string ReceivedResp = "Recieved response from queue service.";

        public const string SendingMessageToQueue = "Sending message to queue service.";

    }
}