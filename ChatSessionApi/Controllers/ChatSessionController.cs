namespace ChatSessionApi.Controllers
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Interfaces;

    [ApiController]
    [Route("api/[controller]")]
    public class ChatSessionController : ControllerBase
    {
        public ChatSessionController(IChatSessionService mng)
         {
             Manager = mng;
         }

        private IChatSessionService Manager { get; }

        [HttpPost]
         public async Task<ActionResult> CreateSession()
         {
             var res = await Manager.CreateNewChatSession();
             if (res.IsSuccess) return Ok();
        
             return StatusCode(400, res.Error);
         }
    }
}