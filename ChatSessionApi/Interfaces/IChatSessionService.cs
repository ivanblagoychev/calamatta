﻿namespace ChatSessionApi.Interfaces
{
    using Shared.Models.ResponseModels;
    using System.Threading.Tasks;
    using Models.ViewModels;
    
    public interface IChatSessionService
    {
        Task<ApiResponse> CreateNewChatSession();
    }
}