﻿namespace ChatSessionApi.Interfaces
{
    using Shared.Models;
    using System.Threading.Tasks;
    
    public interface IDbOperator
    {
        Task<long> CreateRequestChatSession(ChatSession session);
    }
}