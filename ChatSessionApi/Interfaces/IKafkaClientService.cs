﻿namespace ChatSessionApi.Interfaces
{
    using Shared.Models;
    using Shared.Models.ResponseModels;
    using System.Collections.Concurrent;
    using System.Threading.Tasks;
    
    public interface IKafkaClientService
    {
        Task SendChatSessionCreateReq(ChatSession session);
        
        ConcurrentDictionary<long?, ApiResponse> PendingResponse { get; set; }
    }
}