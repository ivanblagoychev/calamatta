﻿namespace ChatSessionApi.Services
{
    using Settings;
    using Microsoft.Extensions.Options;
    using Shared.Models;
    using Shared.Models.ResponseModels;
    using Interfaces;
    using Newtonsoft.Json;
    using System.Collections.Concurrent;
    using System.Threading.Tasks;
    using Confluent.Kafka;

    public class KafkaClientService : IKafkaClientService
    {
        public KafkaClientService(IOptions<KafkaClientSettings> settings)
        {
            Settings = settings.Value;
        }
        
        private KafkaClientSettings Settings { get; } 
        
        public ConcurrentDictionary<long?, ApiResponse> PendingResponse { get; set; } = new ConcurrentDictionary<long?, ApiResponse>();

        public async Task SendChatSessionCreateReq(ChatSession session)
        {
            var config = new ProducerConfig
            {
                BootstrapServers = Settings.Server
            };

            var msg = JsonConvert.SerializeObject(session);
            
            using (var producer = new ProducerBuilder<Null, string>(config).Build())
            {
                await producer.ProduceAsync(Settings.Topic, new Message<Null, string> { Value= msg });
                producer.Flush();
            }
        }
    }
}