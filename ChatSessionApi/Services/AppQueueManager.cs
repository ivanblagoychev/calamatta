﻿namespace ChatSessionApi.Services
{
    using Constants;
    using Shared;
    using Shared.Models.ExceptionsModels;
    using Shared.Enums;
    using Shared.Models;
    using Shared.Models.ResponseModels;
    using System.Linq;
    using Interfaces;
    using System;
    using System.Threading.Tasks;
    
    public class ChatSessionService : IChatSessionService
    {
        public ChatSessionService(IDbOperator dbOperator, IKafkaClientService kService, ISimpleLogger log)
        {
            DbOperator = dbOperator;
            Kafka = kService;
            Logger = log;
        }
        
        private IKafkaClientService Kafka { get; set; }
        
        private IDbOperator DbOperator { get; set; }
        
        private ISimpleLogger Logger { get; }
        
        public async Task<ApiResponse> CreateNewChatSession()
        {
            try
            {
                Logger.LogInfo(Info.ReceivedNewChatSession);
                var newChatSession = new ChatSession() { Status = ChatSessionStatus.Requested, RequestedAt = DateTime.Now};
                newChatSession.Id = await DbOperator.CreateRequestChatSession(newChatSession);

                Logger.LogInfo(Info.SendingMessageToQueue);
                await Kafka.SendChatSessionCreateReq(newChatSession);

                while (true)
                {
                    if (Kafka.PendingResponse.Any(x => x.Key == newChatSession.Id))
                        break;
                    await Task.Delay(2000);
                }

                Logger.LogInfo(Info.ReceivedResp);
                ApiResponse resp = Kafka.PendingResponse
                    .FirstOrDefault(x => x.Key == newChatSession.Id).Value;

                return resp;
            }
            catch (Exception e)
            {
                Logger.LogInfo(e.Message);
                return new ApiResponse(){Error = new ApiException(e.Message, null)};
            }
        }
    }
}