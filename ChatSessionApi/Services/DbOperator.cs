﻿namespace ChatSessionApi.Services
{
    using Shared.Models;
    using Settings;
    using Microsoft.Extensions.Options;
    using System;
    using System.Threading.Tasks;
    using Constants;
    using Interfaces;
    using Npgsql;
    using Dapper;
    
    public class DbOperator : IDbOperator
    {
        public DbOperator(IOptions<DbSettings> settings)
        {
            Settings = settings.Value;
        }
        
        private DbSettings Settings { get; }
        
        public async Task<long> CreateRequestChatSession(ChatSession session)
        {
            var query = Queries.CreateNewChatSession;

            using (var conn = new NpgsqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                using (var tran = conn.BeginTransaction())
                {
                    var id = Convert.ToInt64(await conn.ExecuteScalarAsync(query, new
                    {
                        status = (int) session.Status,
                        requestedAt = DateTime.Now
                    }));

                    tran.Commit();

                    return id;
                }
            }
        }
    }
}