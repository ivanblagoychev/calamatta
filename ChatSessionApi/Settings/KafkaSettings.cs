﻿namespace ChatSessionApi.Settings
{
    public class KafkaSettings
    {
        public string Server { get; set; }
        
        public string GroupId { get; set; }
    }

    public class KafkaConsumerSettings : KafkaSettings
    {
        public string Topic { get; set; }
    }

    public class KafkaClientSettings : KafkaSettings
    {
        public string Topic { get; set; }
    }
}