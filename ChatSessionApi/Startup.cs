namespace ChatSessionApi
{
    using Shared.Extensions;
    using Shared;
    using Shared.Services;
    using Enum;
    using Settings;
    using BackgroundJobs;
    using Dapper;
    using System.Text.Json.Serialization;
    using Interfaces;
    using Services;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    
    public class Startup
    {
        public Startup(IConfiguration config)
        {
            Configuration = config;
        }
        
        private IConfiguration Configuration { get; set; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            
            services.AddMvc().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.IgnoreNullValues = true;
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            });

            services.Configure<DbSettings>(Configuration.GetSection(ConfigSection.DbSettings.ToString()))
                .Configure<KafkaConsumerSettings>(Configuration.GetSection(ConfigSection.KafkaConsumer.ToString()))
                .Configure<KafkaClientSettings>(Configuration.GetSection(ConfigSection.KafkaClient.ToString()));

            services
                //.AddHostedService<KafkaConsumerJob>()
                .AddTransient<IChatSessionService, ChatSessionService>()
                .AddTransient<IDbOperator, DbOperator>()
                .AddSingleton<IKafkaClientService, KafkaClientService>()
                .AddTransient<ISimpleLogger, SimpleLogger>()
                .AddHostedService<KafkaConsumerJob>();
            
            DefaultTypeMap.MatchNamesWithUnderscores = true;
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();
            app.UseEndpoints(e => e.MapControllers());
            app.UseUnhandledExceptionHandling();
        }
    }
}