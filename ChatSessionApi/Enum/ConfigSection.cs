﻿namespace ChatSessionApi.Enum
{
    public enum ConfigSection
    {
        DbSettings,
        KafkaConsumer,
        KafkaClient
    }
}