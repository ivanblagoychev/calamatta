﻿namespace Shared.Enums
{
    public enum Team
    {
        A = 1,
        B = 2,
        C = 3,
        Overflow = 4
    }
}