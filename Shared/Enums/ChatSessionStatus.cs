﻿namespace Shared.Enums
{
    public enum ChatSessionStatus
    {
        Requested = 0,
        Queued = 1,
        Assigned = 2,
        Closed = 3,
        Denied = 4
    }
}