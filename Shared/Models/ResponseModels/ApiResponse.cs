﻿namespace Shared.Models.ResponseModels
{
    using ExceptionsModels;
    
    public class ApiResponse
    {
        public bool IsSuccess { get; set; }
        
        public long? ChatSessionId { get; set; }
        
        public ApiException Error { get; set; }
    }
}