﻿namespace Shared.Models.ExceptionsModels
{
    using System;

    public class ApiException : Exception
    {
        public ApiException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}