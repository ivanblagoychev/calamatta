﻿namespace Shared.Models
{
    using System;
    using Enums;
    
    public class ChatSession
    {
        public ChatSession()
        { }
        
        public long Id { get; set; }

        public DateTime RequestedAt { get; set; }
        
        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public DateTime ClosedAt { get; set; }

        public ChatSessionStatus Status { get; set; }
    }
}