﻿namespace Shared.Extensions
{
    using Microsoft.AspNetCore.Diagnostics;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.Extensions.DependencyInjection;
    using System.Net;
    
    public static class ExceptionHandlingMiddleware
    {
        public static void UseUnhandledExceptionHandling(this IApplicationBuilder builder)
        {
            builder.UseExceptionHandler(options =>
            {
                options.Run(async context =>
                {
                    context.Response.StatusCode = (int) HttpStatusCode.BadRequest;
                    context.Response.ContentType = "application/json";
                    var exceptionObject = context.Features.Get<IExceptionHandlerFeature>();

                    if (exceptionObject != null)
                    {
                        string innerExceptionMessage = exceptionObject.Error.InnerException == null
                            ? "No inner exception"
                            : exceptionObject.Error.InnerException.Message;
                        var errorMessage =
                            $"Error: {exceptionObject.Error.Message}\n{innerExceptionMessage}\n{exceptionObject.Error.StackTrace}";

                        using (var scope = builder.ApplicationServices.CreateScope())
                        {
                            var logger = builder.ApplicationServices.GetRequiredService<ISimpleLogger>();
                            logger.LogInfo($"[{context.TraceIdentifier}]: Unhandled exception occured:\n{errorMessage}\n");
                        }
                    }
                });
            });
        }
    }
}