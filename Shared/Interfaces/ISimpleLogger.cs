﻿namespace Shared
{
    public interface ISimpleLogger
    {
        void LogInfo(string message);
    }
}