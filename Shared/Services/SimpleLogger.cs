﻿namespace Shared.Services
{
    using Microsoft.Extensions.Logging;
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    
    public class SimpleLogger : ISimpleLogger
    {
        private object infoLocker = new { };

        public SimpleLogger(ILogger<SimpleLogger> logger)
        {
            Logger = logger;
        }

        private ILogger<SimpleLogger> Logger { get; set; }

        public void LogInfo(string message)
        {
            Logger.LogInformation(message);
            
            string path = "Logs";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);


            string[] filesInPath = Directory.GetFiles(path).Where(f => f.IndexOf($"{DateTime.Now.Date:yyyyMMdd}") != -1).ToArray();

            var sb = new StringBuilder();
            var now = DateTime.Now;

            while (true)
            {
                try
                {
                    if (!filesInPath.Any())
                    {
                        sb.Append($"[{now:yyyy-MM-dd HH:mm:ss}] -- ").AppendLine(message);
                        var log = sb.ToString();

                        lock (infoLocker)
                        {
                            File.AppendAllText($"{path}/ChatSessionServices-{now:yyyyMMddHHmmss}.csv", log);
                        }

                        break;
                    }
                    else
                    {
                        var currentFile = filesInPath.OrderByDescending(x => x).FirstOrDefault();

                        var currentFileLength = new FileInfo($"{currentFile}").Length;

                        if (currentFileLength >= 5 * 1024 * 1024)
                        {
                            currentFile = $"{path}/ChatSessionServices-{now:yyyyMMddHHmmss}.csv";
                        }

                        sb.Append($"[{now:yyyy-MM-dd HH:mm:ss}] -- ").AppendLine(message);
                        var log = sb.ToString();

                        lock (infoLocker)
                        {
                            File.AppendAllText($"{currentFile}", log);
                        }

                        break;
                    }
                }
                catch (IOException)
                {
                    Task.Delay(30);
                }
            }
        }
    }
}