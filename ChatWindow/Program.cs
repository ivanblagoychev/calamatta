﻿namespace ChatWindow
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    
    class Program
    {
        static async Task Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Please type in 'Create' to create chat session.");
                var cmd = Console.ReadLine();
                if (cmd == "Create")
                {
                    HttpClient client = new HttpClient();
                    var msg = await client.PostAsync("http://localhost:4072/api/ChatSession", new StringContent(" "));
                    
                    if (msg.StatusCode == HttpStatusCode.OK)
                        Console.WriteLine("Chat session created successfully.");
                    else Console.WriteLine("Chat session creation failed.");
                }
                else Console.WriteLine("Wrong command.");
            }
        }
    }
}