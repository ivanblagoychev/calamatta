﻿namespace SenssionQueueApi.Enums
{
    public enum ConfigSection
    {
        Multipliers,
        TeamsAgentsInfo,
        WorkingHours,
        DbSettings,
        KafkaConsumerAgent,
        KafkaConsumerChat,
        KafkaClient
    }
}