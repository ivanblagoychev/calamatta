﻿namespace SenssionQueueApi.Models
{
    using Shared.Models;

    public class QueueAgentRequest
    {
        public ChatSession Session { get; set; }
        
        public long SessionId { get; set; }
        
        public int TeamId { get; set; }
    }
}