﻿namespace SenssionQueueApi.Models.ViewModels
{
    using Shared.Enums;
    
    public class EnqueueSessionViewModel
    {
        public long ChatSessionId { get; set; }
        
        public ChatSessionStatus Status { get; set; }
    }
}