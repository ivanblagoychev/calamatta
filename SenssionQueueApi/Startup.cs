namespace SenssionQueueApi
{
    using Shared.Extensions;
    using Shared;
    using Shared.Services;
    using Enums;
    using BackgroundJobs;
    using Microsoft.Extensions.Configuration;
    using Settings;
    using System.Text.Json.Serialization;
    using Dapper;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.DependencyInjection;
    using Interfaces;
    using Services;
    
    public class Startup
    {
        public Startup(IConfiguration cfg)
        {
            Configuration = cfg;
        }
        
        private IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            
            services.AddMvc().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.IgnoreNullValues = true;
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            });
            
            services.Configure<MultipliersSettings>(Configuration.GetSection(ConfigSection.Multipliers.ToString()))
                .Configure<WorkHoursSettings>(Configuration.GetSection(ConfigSection.WorkingHours.ToString()))
                .Configure<DbSettings>(Configuration.GetSection(ConfigSection.DbSettings.ToString()))
                .Configure<KafkaConsumerAgentSettings>(Configuration.GetSection(ConfigSection.KafkaConsumerAgent.ToString()))
                .Configure<KafkaConsumerChatSettings>(Configuration.GetSection(ConfigSection.KafkaConsumerChat.ToString()))
                .Configure<KafkaClientSettings>(Configuration.GetSection(ConfigSection.KafkaClient.ToString()));

            services.AddHostedService<QueueTeamManagementJob>()
                .AddHostedService<KafkaConsumerJob>();
            
            services.AddSingleton<IQueueManagerService, QueueManagerService>()
                .AddSingleton<IQueueProvider, QueueProvider>()
                .AddSingleton<IApplicationQueue, ApplicationQueue>()
                .AddTransient<IDbOperator, DbOperator>()
                .AddTransient<IKafkaClientService, KafkaClientService>()
                .AddTransient<ISimpleLogger, SimpleLogger>();
            
            DefaultTypeMap.MatchNamesWithUnderscores = true;
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();
            app.UseEndpoints(e => e.MapControllers());
            app.UseUnhandledExceptionHandling();
        }
    }
}