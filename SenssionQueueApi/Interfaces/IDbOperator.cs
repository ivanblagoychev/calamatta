﻿namespace SenssionQueueApi.Interfaces


{
    using System.Threading.Tasks;
    
    public interface IDbOperator
    {
        Task UpdateChatSessionStatusToQued(long sessionId);
    }
}