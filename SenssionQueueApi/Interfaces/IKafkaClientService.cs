﻿namespace SenssionQueueApi.Interfaces
{
    using System.Threading.Tasks;
    using Shared.Models;
    using Shared.Models.ResponseModels;

    public interface IKafkaClientService
    {
        Task SendQueuedSessionForAssign(ChatSession session);

        //void SendChatSessionForClosing(List<int> agentsIds);
        
        Task SendAgentServiceResult(ApiResponse response);

        Task SendSessionClosingReqForTeam(int teamId);

        Task SendQueueChatSessionRequestError(ApiResponse resp);
    }
}