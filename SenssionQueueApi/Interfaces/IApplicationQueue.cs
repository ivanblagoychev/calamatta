﻿namespace SenssionQueueApi.Interfaces
{
    using Shared.Models;
    using Shared.Models.ResponseModels;
    
    public interface IApplicationQueue
    {
        void AddNewChatSession(ChatSession session, ref ApiResponse resp);

        void AddNewChatSessionToOverflow(ChatSession session, ref ApiResponse resp);

        int SessionQueueCapacity { get; set; }
        
        int OverflowQueueCapacity { get; set; }

        void SetMaxQueueLength();

        void ClearQueues();
    }
}