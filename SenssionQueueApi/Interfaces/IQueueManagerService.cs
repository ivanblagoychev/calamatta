﻿namespace SenssionQueueApi.Interfaces
{
    using System.Threading.Tasks;
    using Shared.Models;
    using Shared.Models.ResponseModels;
    
    public interface IQueueManagerService
    {
        Task AddChatSessionToQueue(ChatSession session);

        Task ProcessMessageFromAgentApi(ApiResponse response);

        Task CloseCurrentTeamSessions(int currentTeamId);
    }
}