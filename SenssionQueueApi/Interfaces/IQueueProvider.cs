﻿namespace SenssionQueueApi.Interfaces
{
    public interface IQueueProvider
    {
        IApplicationQueue Queue { get; } 
    }
}