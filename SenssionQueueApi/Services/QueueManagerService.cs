﻿namespace SenssionQueueApi.Services
{
    using System.Threading.Tasks;
    using Shared;
    using Shared.Models.ResponseModels;
    using System;
    using Microsoft.Extensions.Options;
    using Settings;
    using Interfaces;
    using Shared.Models;

    public class QueueManagerService : IQueueManagerService
    {
        public QueueManagerService(IOptions<WorkHoursSettings> settings, IDbOperator dbOperator, IKafkaClientService kafka, IQueueProvider queueProvider, ISimpleLogger log)
        {
            Settings = settings.Value;
            DbOperator = dbOperator;
            KafkaService = kafka;
            QueueProvider = queueProvider;
            Logger = log;
        }
        
        private WorkHoursSettings Settings { get; }
        
        private IDbOperator DbOperator { get; }
        
        private IKafkaClientService KafkaService { get; }
        
        private IQueueProvider QueueProvider { get; set; }

        private ISimpleLogger Logger { get; }
        
        public async Task AddChatSessionToQueue(ChatSession session)
        {
            try
            {
                var resp = new ApiResponse() { ChatSessionId = session.Id }; 
                QueueProvider.Queue.AddNewChatSession(session, ref resp);

                if (!resp.IsSuccess && IsBetweenWorkHours())
                {
                    resp = new ApiResponse() { ChatSessionId = session.Id };
                    QueueProvider.Queue.AddNewChatSessionToOverflow(session, ref resp);
                }

                if (resp.IsSuccess)
                {
                    await DbOperator.UpdateChatSessionStatusToQued(session.Id);
                    await KafkaService.SendQueuedSessionForAssign(session);
                }
                else await KafkaService.SendQueueChatSessionRequestError(resp);
            }
            catch (Exception e)
            {
                Logger.LogInfo(e.Message);
            }
        }

        public async Task ProcessMessageFromAgentApi(ApiResponse response)
        {
            try
            {
                await KafkaService.SendAgentServiceResult(response);
            }
            catch (Exception e)
            {
                Logger.LogInfo(e.Message);
            }
        }
        
        public async Task CloseCurrentTeamSessions(int currentTeamId)
        {
            try
            {
                await KafkaService.SendSessionClosingReqForTeam(currentTeamId);
            
                QueueProvider.Queue.ClearQueues();
                QueueProvider.Queue.SetMaxQueueLength();
            }
            catch (Exception e)
            {
                Logger.LogInfo(e.Message);
            }
        }

        private bool IsBetweenWorkHours()
        {
            var hourFrom = TimeSpan.Parse(Settings.WorkingHoursFrom);
            var hourTo = TimeSpan.Parse(Settings.WorkingHoursTo);
            
            return DateTime.Now.TimeOfDay >= hourFrom && DateTime.Now.TimeOfDay <= hourTo;
        }
    }
}