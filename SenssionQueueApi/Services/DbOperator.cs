﻿namespace SenssionQueueApi.Services
{
    using System.Threading.Tasks;
    using System;
    using Dapper;
    using Npgsql;
    using Constants;
    using Shared.Enums;
    using Interfaces;
    using Microsoft.Extensions.Options;
    using Settings;
    
    public class DbOperator : IDbOperator
    {
        public DbOperator(IOptions<DbSettings> settings)
        {
            Settings = settings.Value;
        }
        
        private DbSettings Settings { get; }
        
        public async Task UpdateChatSessionStatusToQued(long sessionId)
        {
            var query = Queries.ChangeChatSessionStatus;

            using (var conn = new NpgsqlConnection(Settings.ConnectionString))
            {
                conn.Open();

                using (var tran = conn.BeginTransaction())
                {
                    await conn.ExecuteAsync(query, new
                    {
                        status = (int) ChatSessionStatus.Queued,
                        id = sessionId,
                        queued_at = DateTime.Now
                    });

                    tran.Commit();
                }
            }
        }
    }
}