﻿namespace SenssionQueueApi.Services
{
    using Interfaces;
    
    public  class QueueProvider : IQueueProvider
    {
        public QueueProvider(IApplicationQueue queue)
        {
            Queue = queue;
        }
        public IApplicationQueue Queue { get; } 
    }
}