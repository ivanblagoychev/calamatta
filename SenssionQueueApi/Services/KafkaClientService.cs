﻿namespace SenssionQueueApi.Services
{
    using Constants;
    using System.Threading.Tasks;
    using Shared;
    using Microsoft.Extensions.Options;
    using Settings;
    using Confluent.Kafka;
    using Newtonsoft.Json;
    using Models;
    using Interfaces;
    using Shared.Models;
    using Shared.Models.ResponseModels;
    
    public class KafkaClientService : IKafkaClientService 
    {
        public KafkaClientService(IOptions<KafkaClientSettings> settings,ISimpleLogger logg)
        {
            Settings = settings.Value;
            Logger = logg;
        }
        
        private KafkaClientSettings Settings { get; }
        
        private ISimpleLogger Logger { get; set; }
        
        public async Task SendQueuedSessionForAssign(ChatSession session)
        {
            var config = new ProducerConfig
            {
                BootstrapServers = Settings.Server
            };

            var msg = JsonConvert.SerializeObject(new QueueAgentRequest(){ Session = session});
            
            using (var producer = new ProducerBuilder<Null, string>(config).Build())
            {
                Logger.LogInfo(Info.SendingMessageToAgentTryAssign);
                await producer.ProduceAsync(Settings.QueueToAgentReqTopic, new Message<Null, string> { Value= msg });
                producer.Flush();
            }
        }
        
        public async Task SendChatSessionForClosing(long sessionId)
        {
            var config = new ProducerConfig
            {
                BootstrapServers = Settings.Server
            };

            var msg = JsonConvert.SerializeObject(new QueueAgentRequest(){ SessionId = sessionId });
            
            using (var producer = new ProducerBuilder<Null, string>(config).Build())
            {
                Logger.LogInfo(Info.SendingMessageToAgentCloseSpecific);
                await producer.ProduceAsync(Settings.QueueToAgentReqTopic, new Message<Null, string> { Value= msg });
                producer.Flush();
            }
        }

        public async Task SendAgentServiceResult(ApiResponse response)
        {
            var config = new ProducerConfig
            {
                BootstrapServers = Settings.Server
            };

            var msg = JsonConvert.SerializeObject(response);
            
            using (var producer = new ProducerBuilder<Null, string>(config).Build())
            {
                Logger.LogInfo(Info.SendingMessageToChatResp);
                await producer.ProduceAsync(Settings.QueueToChatResTopic, new Message<Null, string> { Value= msg });
                producer.Flush();
            }
        }

        public async Task SendSessionClosingReqForTeam(int teamId)
        {
            var config = new ProducerConfig
            {
                BootstrapServers = Settings.Server
            };

            var msg = JsonConvert.SerializeObject(new QueueAgentRequest() { TeamId = teamId});
            
            using (var producer = new ProducerBuilder<Null, string>(config).Build())
            {
                Logger.LogInfo(Info.SendingMessageToAgentCloseAll);
                await producer.ProduceAsync(Settings.QueueToAgentReqTopic, new Message<Null, string> { Value= msg });
                producer.Flush();
            }
        }

        public async Task SendQueueChatSessionRequestError(ApiResponse resp)
        {
            var config = new ProducerConfig
            {
                BootstrapServers = Settings.Server
            };

            var msg = JsonConvert.SerializeObject(resp);
            
            using (var producer = new ProducerBuilder<Null, string>(config).Build())
            {
                Logger.LogInfo(Info.SendMessageToChatError);
                await producer.ProduceAsync(Settings.QueueToChatResTopic, new Message<Null, string> { Value= msg });
                producer.Flush();
            }
        }
    }
}