﻿namespace SenssionQueueApi.Services
{
    using System.Collections.Concurrent;
    using Constants;
    using Shared.Models.ExceptionsModels;
    using Shared.Enums;
    using Microsoft.Extensions.Options;
    using Settings;
    using Shared.Models;
    using Shared.Models.ResponseModels;
    using System;
    using Interfaces;
    
    public class ApplicationQueue : IApplicationQueue
    {
        private int sessionMaxCapacity;
        private int overflowMaxCapacity;
        private ConcurrentQueue<ChatSession> sessionQueue;
        private ConcurrentQueue<ChatSession> overflowQueue;

        public ApplicationQueue(IOptions<MultipliersSettings> settings)
        {
            sessionQueue = new ConcurrentQueue<ChatSession>();
            overflowQueue = new ConcurrentQueue<ChatSession>();
            Settings = settings.Value;
            SetMaxQueueLength();
        }
        
        private MultipliersSettings Settings { get; }
        
        private IDbOperator DbOperator { get; set; }
        
        public int SessionQueueCapacity{ get => sessionMaxCapacity; set => sessionMaxCapacity = value; }
        
        public int OverflowQueueCapacity { get => overflowMaxCapacity; set => overflowMaxCapacity = value; }

        
        public void AddNewChatSession(ChatSession session, ref ApiResponse resp)
        {
            if ( sessionQueue.Count >= sessionMaxCapacity)
            {
                resp.IsSuccess = false;
                resp.Error = new ApiException(Info.QueueIsFullMsg, null);
                
                return;
            }
            
            sessionQueue.Enqueue(session);
            resp.IsSuccess = true;
            resp.Error = null;
        }

        public void AddNewChatSessionToOverflow(ChatSession session, ref ApiResponse resp)
        {
            if (overflowMaxCapacity >= overflowQueue.Count)
            {
                resp.IsSuccess = false;
                resp.Error = new ApiException(Info.QueueIsFullMsg, null);
                
                return;
            }
            
            sessionQueue.Enqueue(session);
            resp.IsSuccess = true;
            resp.Error = null;
            DbOperator.UpdateChatSessionStatusToQued(session.Id);
        }

        public void ClearQueues()
        {
            sessionQueue = new ConcurrentQueue<ChatSession>();
            overflowQueue = new ConcurrentQueue<ChatSession>();
        }
        
        public void SetMaxQueueLength()
        {
            var currentTeam = 0;
            
            var now = DateTime.Now.TimeOfDay;
            if (now >= TimeSpan.Parse("00:00:00") && now <= TimeSpan.Parse("07:59:59")) currentTeam = (int) Team.C;
            else if (now >= TimeSpan.Parse("08:00:00") && now <= TimeSpan.Parse("15:59:59")) currentTeam = (int) Team.A;
            else currentTeam = (int) Team.B;
            
            if (currentTeam == 1) SessionQueueCapacity = Convert.ToInt32(((1 * Settings.Concurrency * Settings.TeamLead) + (2 * Settings.Concurrency * Settings.MidLevel) + (1 * Settings.Concurrency * Settings.Junior)) * 1.5);
            else if (currentTeam == 2) SessionQueueCapacity = Convert.ToInt32(((1 * Settings.Concurrency * Settings.Senior) + (1 * Settings.Concurrency * Settings.MidLevel) + (2 * Settings.Concurrency * Settings.Junior)) * 1.5);
            else SessionQueueCapacity = Convert.ToInt32((2 * Settings.Concurrency * Settings.MidLevel) * 1.5);

            OverflowQueueCapacity = Convert.ToInt32((6 * 10 * 0.4) * 1.5);
        }
    }
}