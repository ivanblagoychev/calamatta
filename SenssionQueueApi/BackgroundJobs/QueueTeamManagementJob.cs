namespace SenssionQueueApi.BackgroundJobs
{
    using Shared.Enums;
    using Interfaces;
    using System;
    using System.Diagnostics;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Hosting;

    public class QueueTeamManagementJob : BackgroundService
    {
        public QueueTeamManagementJob(IQueueManagerService manager, IQueueProvider queueProvider)
        {
            Manager = manager;
            QueueProvider = queueProvider;
        }

        private TimeSpan ShiftStart { get; set; }

        private TimeSpan ShiftEnd { get; set; }

        private IQueueManagerService Manager { get; set; }

        private IQueueProvider QueueProvider { get; set; }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Run(async () =>
            {
                while (!stoppingToken.IsCancellationRequested)
                {
                    var currentTeamInt = (int) SelectTheAcurateTeam();
                    double passedMillisecondsShift = ShiftStart.TotalMilliseconds;

                    Stopwatch s = new Stopwatch();
                    s.Start();

                    await Manager.CloseCurrentTeamSessions(currentTeamInt);

                    s.Stop();

                    var totalPassedMilliseconds = passedMillisecondsShift + s.ElapsedMilliseconds;

                    await Task.Delay((int) ShiftEnd.TotalMilliseconds - (int) totalPassedMilliseconds, stoppingToken);
                }
            }, stoppingToken);
        }

        private Team SelectTheAcurateTeam()
        {
            var now = DateTime.Now.TimeOfDay;

            if (now >= TimeSpan.Parse("00:00:00") && now <= TimeSpan.Parse("07:59:59"))
            {
                ShiftStart = new TimeSpan(0, 0, 0);
                ShiftEnd = new TimeSpan(7, 59, 59);
                return Team.C;
            }

            else if (now >= TimeSpan.Parse("08:00:00") && now <= TimeSpan.Parse("15:59:59"))
            {
                ShiftStart = new TimeSpan(8, 0, 0);
                ShiftEnd = new TimeSpan(17, 59, 59);
                return Team.A;
            }

            ShiftStart = new TimeSpan(18, 0, 0);
            ShiftEnd = new TimeSpan(23, 59, 59);
            return Team.B;
        }
    }
}