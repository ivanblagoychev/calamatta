﻿namespace SenssionQueueApi.BackgroundJobs
{
    using Constants;
    using Shared;
    using Shared.Models.ResponseModels;
    using Microsoft.Extensions.Options;
    using Settings;
    using Shared.Models;
    using System.Threading;
    using System.Threading.Tasks;
    using Confluent.Kafka;
    using Microsoft.Extensions.Hosting;
    using Newtonsoft.Json;
    using Interfaces;
    
    public class KafkaConsumerJob : BackgroundService
    {
        public KafkaConsumerJob(IQueueManagerService mngr, IOptions<KafkaConsumerChatSettings> settings,IKafkaClientService kafkaClient, ISimpleLogger logg)
        {
            Manager = mngr;
            Settings = settings.Value;
            Kafka = kafkaClient;
            Logger = logg;
        }
        
        private IQueueManagerService Manager { get; }
        
        private KafkaConsumerChatSettings Settings { get; }
        
        private IKafkaClientService Kafka { get; set; }
        
        private ISimpleLogger Logger { get; set; }
        
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Run( async () =>
            {
                 var config = new ConsumerConfig
                {
                    BootstrapServers = Settings.Server,
                    GroupId = Settings.GroupId,
                    AutoOffsetReset = AutoOffsetReset.Earliest
                };

                using (var consumer = new ConsumerBuilder<Ignore, string>(config).Build())
                {
                    consumer.Subscribe(Settings.ChatToQueueTopic);

                    while (!stoppingToken.IsCancellationRequested)
                    {
                        var consumeResult = consumer.Consume(stoppingToken);

                        Logger.LogInfo(Info.ReceivedMessage);
                        ChatSession session = JsonConvert.DeserializeObject<ChatSession>(consumeResult.Message.Value);

                        if (session.Id != 0)
                            await Manager.AddChatSessionToQueue(session);
                        else
                        {
                            ApiResponse resp = JsonConvert.DeserializeObject<ApiResponse>(consumeResult.Message.Value);
                            await Kafka.SendAgentServiceResult(resp);
                        }
                    }

                    consumer.Close();
                }
            }, stoppingToken);
        }
    }
}