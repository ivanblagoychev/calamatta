﻿namespace SenssionQueueApi.Settings
{
    public class MultipliersSettings
    {
        public double Junior { get; set; }
        
        public double MidLevel { get; set; }
        
        public double Senior { get; set; }
        
        public double TeamLead { get; set; }
        
        public double Concurrency { get; set; }
    }
}