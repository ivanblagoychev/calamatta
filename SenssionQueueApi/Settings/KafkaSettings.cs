﻿namespace SenssionQueueApi.Settings
{
    public class KafkaSettings
    {
        public string Server { get; set; }
        
        public string GroupId { get; set; }
    }
    
    public class KafkaConsumerAgentSettings : KafkaSettings
    {
        public string ChatToQueueTopic { get; set; }
    }

    public class KafkaConsumerChatSettings : KafkaSettings
    {
        public string ChatToQueueTopic { get; set; }
    }

    public class KafkaClientSettings : KafkaSettings
    {
        public string QueueToAgentReqTopic { get; set; }
        
        public string QueueToChatResTopic { get; set; }
    }
}