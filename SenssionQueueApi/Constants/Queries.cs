﻿namespace SenssionQueueApi.Constants
{
    public static class Queries
    {
        public const string ChangeChatSessionStatus = "update public.chat_session set status = @status, queued_at = @queued_at where id = @id;";
    }
}