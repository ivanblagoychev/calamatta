﻿namespace SenssionQueueApi.Constants
{
    public static class Info
    {
        public static string QueueIsFullMsg = "Queue is full.";

        public const string ReceivedMessage = "Recieved message.";

        public const string SendingMessageToAgentTryAssign = "Sending message to agent (try assign a session).";

        public const string SendingMessageToAgentCloseSpecific = "Sending message to agent (close specific session).";
        
        public const string SendingMessageToAgentCloseAll = "Sending message to agent (close specific team).";
        
        public const string SendingMessageToChatResp = "Sending message to chat service with response from agent-queue.";

        public const string SendMessageToChatError = "Sending message to chat service with error response.";
    }
}